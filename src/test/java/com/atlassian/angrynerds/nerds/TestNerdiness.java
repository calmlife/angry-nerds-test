package com.atlassian.angrynerds.nerds;

import org.testng.Assert;
import org.testng.annotations.Test;



public class TestNerdiness {
	
	@Test (groups = {"batch1", "integration_tests", "smoke_test"})
	public void testHasNerdGlasses() throws Exception {
		/////////////////
		// your actual test would go here
		/////////////////
		
		Assert.assertTrue(true);
	}
	
	@Test (groups = {"batch2", "integration_tests", "smoke_test"})
	public void testHasNoSocialLife() throws Exception {
		/////////////////
		// your actual test would go here
		/////////////////
		
		Assert.assertTrue(true);
	}
	
	@Test (groups = {"batch1", "integration_tests"})
	public void testPlaysBoardGames() throws Exception {
		/////////////////
		// your actual test would go here
		/////////////////
		
		Assert.assertTrue(true);
	}
	
	@Test (groups = {"batch1", "integration_tests"})
	public void testBelongsToChessClub() throws Exception {
		/////////////////
		// your actual test would go here
		/////////////////
		
		Assert.assertTrue(true);
	}

}
